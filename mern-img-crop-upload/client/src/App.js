import './App.css';
import { useState } from 'react';
import ImageCrop from './ImageCrop';

function App() {
  const [state, setstate] = useState(null);

  const onSelectFile = e => {
    if (!e.target.files || e.target.files.length === 0) {
        setstate(null)
        return
    }

    // I've kept this example simple by using the first image instead of multiple
    // setstate(e.target.files[0])
    setstate(URL.createObjectURL(e.target.files[0]));
    }
    
  return (
    <div className="App">
      <header className="App-header">
        {/* <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <form>
          accepts only Images
          <img src={state}/>
          <input type="file" onChange={onSelectFile} accept="image/*" />
        </form> */}
        <ImageCrop></ImageCrop>
      </header> 
    </div>
  );
}

export default App;
