import React, { useState, useCallback, useRef, useEffect } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Uploady from "@rpldy/uploady";
import UploadButton from '@rpldy/upload-button';
import axios from 'axios';

var imageUrl;
var imgFile;

function generateDownload(canvas, crop) {

  if (!crop || !canvas) {
    return;
  }

  canvas.toBlob(
    (blob) => {
      const previewUrl = window.URL.createObjectURL(blob);
    // console.log(imageUrl);

    // imgFile = new File([blob], "croppedImage.png", { lastModified : Date.now() });

      const anchor = document.createElement('a');
      anchor.download = 'cropPreview.png';
      anchor.href = URL.createObjectURL(blob);
      anchor.click();

      window.URL.revokeObjectURL(previewUrl);
    },
    'image/png',
    1
  );
}

export default function ImageCrop() { 
  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({ unit: '%', width: 80, aspect: 1 / 1 });
  const [completedCrop, setCompletedCrop] = useState(null);

  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const crop = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = crop.width * pixelRatio;
    canvas.height = crop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );
  }, [completedCrop]);




  async function uploadImage(canvas, crop){

    if (!crop || !canvas) {
      return;
    }
  
    canvas.toBlob(
      (blob) => {
      imageUrl = window.URL.createObjectURL(blob);
     
  
      imgFile = new File([blob], "croppedImage.png", { lastModified : Date.now() });
  
      },
      'image/png',
      1
    );


    let data = new FormData()

    
    data.append('name', 'image')
    
    data.append('file', imgFile);
    console.log(imgFile);
    
    let config = {
      header : {
        'Content-Type' : 'multipart/form-data'
      }
    }

    axios.post('http://localhost:5000/upload', data, config).then(response => {
        console.log('response', response)
      }).catch(error => {
        console.log('error', error)
      })


  }

  return (
    <div className="App">
      <div>
            <input type="file" accept="image/*" onChange={onSelectFile} name="dp"/>
      
            <button onClick={() =>
          uploadImage(previewCanvasRef.current, completedCrop)
        }>Upload Image</button>
      </div>
      <ReactCrop
        src={upImg}
        onImageLoaded={onLoad}
        crop={crop}
        onChange={(c) => setCrop(c)}
        onComplete={(c) => setCompletedCrop(c)}
      />
      <div>
        <canvas
          ref={previewCanvasRef}
          // Rounding is important so the canvas width and height matches/is a multiple for sharpness.
          style={{
            width: Math.round(completedCrop?.width ?? 0),
            height: Math.round(completedCrop?.height ?? 0)
          }}
        />
      </div>
      <button
        type="button"
        disabled={!completedCrop?.width || !completedCrop?.height}
        onClick={() =>
          generateDownload(previewCanvasRef.current, completedCrop)
        }
      >
        Download cropped image
      </button>
      <Uploady
    destination={{ url: "https://my-server/upload" }}>
    <UploadButton/>
</Uploady>
    </div>
  );
}
