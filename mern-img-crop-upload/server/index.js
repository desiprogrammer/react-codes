const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const multer = require('multer');
const bodyparser = require('body-parser');

// The App 
const app = express();
app.use(require('cors')());

app.use(express.static('public'))


// setting up multer diskstorage and name
const storage = multer.diskStorage({
    destination: './public/uploads',
    // function to set the name of file that will be saved in disk 
    filename: function (req, file, cb) {
        console.log(file);
        cb(null, file.originalname.split('.')[0] + Date.now() + path.extname(file.originalname));
    }
});

// the upload method 
const upload = multer({
    storage: storage,
    // filter file extensions 
    fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname);
        if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg') {
            return callback(new Error("Only Images Are Allowed..!"));
        }
        callback(null, true)
    },
    limits: { fileSize: 10000000 }
}).single('file');

// Routes
app.get('/', (req, res) => {
    res.json({ msg: "Welcome !" });

});

// The upload route where files are uploaded 
app.post('/upload', (req, res) => {
    // console.log(req.body);
    // console.log(req.files);
    // res.json(req.body);
    upload(req, res, (err) => {
        if (err) {
            res.json(err);
        } else {
            res.send(req.body);
        }
    });
});

// The Port and app listening to port 
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log("Server Started...");
});
