import { createContext, useState } from "react";
import ContextTaskForm from "./ContextForm";
import ContextTasks from "./ContextTasks";

export const TaskContext = createContext();

export default function ContextHome() {
  const [tasks, setTasks] = useState(["Implement Context"]);

  return (
    <TaskContext.Provider value={{ tasks, setTasks }}>
      <div className="container py-2">
        <h1 className="text-center">Context Tasks App</h1>
        <div className="row justify-content-center align-items-center">
          <div className="col-11 col-md-7 col-lg-5">
            <ContextTaskForm></ContextTaskForm>
            <ContextTasks></ContextTasks>
          </div>
        </div>
      </div>
    </TaskContext.Provider>
  );
}
