import { useContext, useRef } from "react";
import { AddOutline } from "react-ionicons";
import { TaskContext } from "./ContextHome";

export default function ContextTaskForm() {
  const { tasks, setTasks } = useContext(TaskContext);
  const taskref = useRef();

  function handleTask(e) {
    e.preventDefault();
    console.log(taskref.current.value);
    if (taskref.current.value !== "") {
      setTasks([...tasks, taskref.current.value]);
    }
    taskref.current.value = null;
  }

  return (
    <form>
      <div className="mb-3">
        <label htmlFor="task" className="form-label">
          Got a Task ?
        </label>
        <input
          ref={taskref}
          type="text"
          className="form-control form-control-lg"
          id="task"
          placeholder="I want to 🤔"
        ></input>
      </div>
      <div className="d-grid">
        <button onClick={handleTask} className="btn btn-lg btn-primary">
          Add Task
          <AddOutline
            color={"#00000"}
            title={"Add Task"}
            height="32px"
            width="32px"
          />
        </button>
      </div>
    </form>
  );
}
