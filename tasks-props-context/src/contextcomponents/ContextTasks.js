import { useContext } from "react";
import { TrashBin } from "react-ionicons";
import { TaskContext } from "./ContextHome";

export default function ContextTasks() {
  const { tasks, setTasks } = useContext(TaskContext);

  function deleteTask(task) {
    var lefttasks = tasks.filter((anytask) => anytask !== task);
    setTasks(lefttasks);
  }

  return (
    <div style={{ backgroundColor: "#eee" }} className="p-3 mt-3">
      <h3 className="fw-bold text-center text-primary">Tasks</h3>
      {tasks.map((task) => {
        return (
          <div className="row justify-content-between py-2">
            <div className="col">
              <p>{task}</p>
            </div>
            <div className="col-2">
              <TrashBin
                style={{ cursor: "pointer" }}
                onClick={() => deleteTask(task)}
                color={"#E21717"}
                title={"Delete"}
                height="32px"
                width="32px"
              />
            </div>
          </div>
        );
      })}
    </div>
  );
}
