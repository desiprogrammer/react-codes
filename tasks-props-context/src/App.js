import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import PropsHome from "./propcomponents/PropsHome";
import ContextHome from "./contextcomponents/ContextHome";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <div className="main container text-center p-4">
            <h1>Welcome To React Tasks App</h1>
            <p>
              Contains 2 apps, One coded using simple props and another using
              Context !
            </p>
            <Link to="/props" className="btn btn-lg btn-outline-primary my-3">
              Using Props
            </Link>
            <Link to="/context" className="btn btn-lg btn-primary">
              Using Context
            </Link>
          </div>
        </Route>
      </Switch>
      <Switch>
        <Route exact path="/props">
        <PropsHome></PropsHome>
        </Route>
      </Switch>
      <Switch>
        <Route exact path="/context">
          <ContextHome></ContextHome>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
