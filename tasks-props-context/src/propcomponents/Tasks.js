import Task from "./Task";

export default function Tasks({ tasks, deleteTask }) {
  return (
    <div style={{ backgroundColor: "#eee" }} className="p-3 mt-3">
      <h3 className="fw-bold text-center">Tasks</h3>
      {tasks.map((task) => {
        return <Task key={task} deleteTask={deleteTask} task={task}></Task>;
      })}
    </div>
  );
}
