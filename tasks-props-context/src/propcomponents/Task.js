import { TrashBinOutline } from "react-ionicons";

export default function Task({ task, deleteTask }) {
  return (
    <div className="row justify-content-between py-2">
      <div className="col">
        <p>{task}</p>
      </div>
      <div className="col-2">
        <TrashBinOutline
          style={{ cursor: "pointer" }}
          onClick={() => deleteTask(task)}
          color={"#E21717"}
          title={"Delete"}
          height="32px"
          width="32px"
        />
      </div>
    </div>
  );
}
