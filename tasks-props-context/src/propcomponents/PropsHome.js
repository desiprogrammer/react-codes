import { useState } from "react";
import TaskForm from "./Taskform";
import Tasks from "./Tasks";

export default function PropsHome() {
  const [tasks, setTasks] = useState(["Implement Props"]);

  var addTask = (task) => {
    setTasks([...tasks, task]);
  };

  var deleteTask = (task) => {
    var newTasks = tasks.filter((anytask) => anytask !== task);
    setTasks(newTasks);
  };

  return (
    <div className="container py-2">
      <h1 className="text-center">Props Tasks App</h1>
      <div className="row justify-content-center align-items-center">
        <div className="col-11 col-md-7 col-lg-5">
          <TaskForm addTask={addTask}></TaskForm>
          <Tasks tasks={tasks} deleteTask={deleteTask}></Tasks>
        </div>
      </div>
    </div>
  );
}
