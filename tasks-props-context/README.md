# `Tasks app in React Coded Using Props and Context`

# `Contents`
    A task app using props
    A task app using context

# `Using This`

```bash
    git clone https://gitlab.com/desiprogrammer/react-codes.git react-codes
    cd react-codes/tasks-props-context
    npm install
    # visit http://localhost:3000
```

