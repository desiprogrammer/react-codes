import "./App.css";
import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/Login";
import Register from "./components/Signup";
import { useEffect, useState } from "react";

function App() {
 
  const [joke, setjoke] = useState('fetching...');

  const getJoke = () => {
    fetch("https://official-joke-api.appspot.com/jokes/programming/random")
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result);
        setjoke(result[0].setup);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  useEffect(() => {
    console.log("Calling API !");
    getJoke();
  }, []);


  return (
    <Router>
      <Navbar></Navbar>
      <Switch>
        <Route exact path="/">
          <div className='text-center'>
          <h1 className="text-center py-5">Welcome To Home Page</h1>
          <p>Here is a joke For You</p>
          <p className='text-primary'> {joke}</p>
          </div>
        </Route>
      </Switch>
      <Switch>
        <Route exact path="/login">
          <Login></Login>
        </Route>
      </Switch>
      <Switch>
        <Route exact path="/register">
          <Register></Register>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
