import './App.css';
import StyledButtons from './StyledButtons';

function App() {

  const buttonStyle = {
    'padding' : '12px 32px',
    'fontSize' : '20px',
    'backgroundColor' : '#1c1c1c',
    'border' : 'none',
    'color' : '#eee',
    'margin' : '10px',
  };

  return (
    <div className="App">
      <h1>Styled Components</h1>
      <p>There are different ways of styling in React.</p>
      <ul>
        <li>className : can put simple css in CSS files and add them as classnames</li>
        <li>Inline Styling : Putting inline Styling using the Style Prop</li>
        <li>Usign styled Components</li>
      </ul>
      <button style={buttonStyle}>Button Without Styled Components</button>
      <StyledButtons bg={'purple'}>Button With Styled-Components</StyledButtons>
    </div>
  );
}

export default App;
