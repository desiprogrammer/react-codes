import styled from 'styled-components';

export default styled.button`
    background: ${props => props.bg};
    border : none;
    border-radius: 3px;
    color: white;
    margin: 0 1em;
    font-size : 20px;
    padding: 1em 1em;
    transition : all ease-in 300ms;
    cursor: pointer;

    &:hover {
        background : blue;
        transition : all ease-in 500ms;
    }

    @media only screen and (max-width: 600px) {
        background-color: blue;
    }
`;