# `React Codes`

# `Contents`
    A task app using props
    A task app using context
    A Material UI Login Screen With Customization Form Validation
    A Bootstrap UI With Form Validation
    React Styled Components Demo
# `Using This`

```bash
    git clone https://gitlab.com/desiprogrammer/react-codes.git react-codes
    cd <whatever you want>
    npm install
    # visit http://localhost:3000
```

# Screenshots

### `Task App`
![Task App](Screenshots/tasks-app.png)

### `Bootstrap Login UI`
![Task App](Screenshots/bootstrap-ui.png)