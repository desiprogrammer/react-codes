import {
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core";
import { indigo } from '@material-ui/core/colors';
import Login from "./login/Login";

function App() {
  const theme = createMuiTheme({
    palette: {
      primary: {
        main: indigo[500],
      },
    },
    typography: {
      fontFamily: [
        'Josefin Sans',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
      ].join(','),
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <Login />
    </ThemeProvider>
  );
}

export default App;


