import React from "react";
import {
  Avatar,
  Grid,
  Paper,
  Typography,
  TextField,
  Button,
  Link,
  Box,
} from "@material-ui/core";
import { AccountCircle } from "@material-ui/icons";
import * as Yup from "yup";

import { useFormik } from "formik";

export default function Login() {
  const paperStyle = { padding: "24px", borderRadius: "5px" };
  const bottomMarginStyle = { marginBottom: "12px" };
  const buttonStyle = { padding: "10px 0px", fontSize: "20px" };
  const preventDefault = (event) => event.preventDefault();

  // should be same as of Input Field's Name

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email("Please Enter Valid Email")
      .required("Required *"),
    password: Yup.string().min(6).max(15).required("Required *"),
  });

  //   const WithMaterialUI = () => {dire
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });
  // }

  return (
    <Grid
      container
      style={{ margin: "100px auto" }}
      direction="column"
      alignItems="center"
      justify="center"
      xl="4"
      md="8"
      sm="11"
      spacing={0}
    >
      <Paper elevation={10} style={paperStyle} alignItems="center">
        <Grid item align="center" style={bottomMarginStyle}>
          <Avatar style={{ backgroundColor: "green", height: '68px', width: '68px' }}>
            <AccountCircle style={{ fontSize: 68, padding: "8px" }} />
          </Avatar>
          <Typography variant="h4"> Sign In </Typography>
        </Grid>
        <form onSubmit={formik.handleSubmit}>
          <TextField
            fullWidth
            id="email"
            name="email"
            label="Email"
            variant="filled"
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
            style={bottomMarginStyle}
          />
          <TextField
            fullWidth
            id="password"
            name="password"
            label="Password"
            type="password"
            variant="filled"
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
            style={bottomMarginStyle}
          />
          <Button
            color="primary"
            variant="contained"
            fullWidth
            type="submit"
            style={buttonStyle}
          >
            Submit
          </Button>
        </form>
        <Grid item align="start">
          <Box py={1}>
            <Typography>
              <Link href="#" onClick={preventDefault}>
                Forgot Password ?
            </Link>
            </Typography>
          </Box>
          <Typography>
            New Here
            <Link style={{ 'paddingLeft': '4px' }} href="#" onClick={preventDefault}>
              Register Now
            </Link>
          </Typography>
        </Grid>
      </Paper>
    </Grid>
  );
}
